<DPSerialization>
 <complexType xsi:type="genericCheckpointing.util.MyAllTypesFirst">
  < myOtherLong xsi:type="xsi:long">3000</myOtherLong>
  < myString xsi:type="xsi:string">Serial String 0</myString>
  < myBool xsi:type="xsi:boolean">true</myBool>
 </complexType>
</DPSerialization>
<DPSerialization>
 <complexType xsi:type="genericCheckpointing.util.MyAllTypesSecond">
  < myOtherDoubleT xsi:type="xsi:double">40.0</myOtherDoubleT>
  < myFloatT xsi:type="xsi:float">2.0</myFloatT>
  < myShortT xsi:type="xsi:short">2</myShortT>
  < myCharT xsi:type="xsi:char">u</myCharT>
 </complexType>
</DPSerialization>
<DPSerialization>
 <complexType xsi:type="genericCheckpointing.util.MyAllTypesFirst">
  < myLong xsi:type="xsi:long">3000</myLong>
  < myOtherLong xsi:type="xsi:long">2000</myOtherLong>
  < myString xsi:type="xsi:string">Serial String 1</myString>
  < myBool xsi:type="xsi:boolean">false</myBool>
 </complexType>
</DPSerialization>
<DPSerialization>
 <complexType xsi:type="genericCheckpointing.util.MyAllTypesSecond">
  < myDoubleT xsi:type="xsi:double">10.0</myDoubleT>
  < myOtherDoubleT xsi:type="xsi:double">41.0</myOtherDoubleT>
  < myFloatT xsi:type="xsi:float">6.0</myFloatT>
  < myShortT xsi:type="xsi:short">3</myShortT>
  < myCharT xsi:type="xsi:char">w</myCharT>
 </complexType>
</DPSerialization>
<DPSerialization>
 <complexType xsi:type="genericCheckpointing.util.MyAllTypesFirst">
  < myOtherInt xsi:type="xsi:int">11</myOtherInt>
  < myLong xsi:type="xsi:long">6000</myLong>
  < myOtherLong xsi:type="xsi:long">1000</myOtherLong>
  < myString xsi:type="xsi:string">Serial String 2</myString>
  < myBool xsi:type="xsi:boolean">true</myBool>
 </complexType>
</DPSerialization>
<DPSerialization>
 <complexType xsi:type="genericCheckpointing.util.MyAllTypesSecond">
  < myDoubleT xsi:type="xsi:double">20.0</myDoubleT>
  < myOtherDoubleT xsi:type="xsi:double">42.0</myOtherDoubleT>
  < myFloatT xsi:type="xsi:float">10.0</myFloatT>
  < myShortT xsi:type="xsi:short">4</myShortT>
  < myCharT xsi:type="xsi:char">t</myCharT>
 </complexType>
</DPSerialization>
